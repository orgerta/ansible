resource "azurerm_linux_virtual_machine" "linuxvm" {
  name = "orgerta-vm"
  resource_group_name = "orgerta_rg"
  location = "westeurope"
  admin_username = "azureuser"
  computer_name = "orgerta-vm"
  network_interface_ids = ["/subscriptions/c0177cf4-3641-4a06bbf6-40d1ac0a60f8/resourceGroups/orgerta_rg/providers/Microsoft.Network/networkInterfaces/orgerta-vm192"]
    os_disk  {
    caching = "ReadWrite"

    disk_size_gb = 30

    name = "orgerta-vm_OsDisk_1_a4869a1ba6fd495d9073e4fda9dd7eb9"

    
    storage_account_type = "Premium_LRS"

    write_accelerator_enabled = false
  }
    source_image_reference {
        offer = "0001-com-ubuntu-server-focal"

        publisher  = "canonical"

        sku  = "20_04-lts-gen2"

        version = "latest"
  }
    admin_ssh_key {
           public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQChZcX725NKTECbmripTd9rI+wGcsVx2nrFJFNEDbssxnK3CS+hX1eGZBbgG809QOiQVbadKABv9rnu+nLnjdxdL+wyb3Nz2zjg5h0BoFuZE7uoEb8kDe55sV+5dv1PE4lA8TXIUhLCGL+7pb8NUBHkazE+3suTKBVP8FhpLox7WG/NbzbpR4yTc6fLGgyLxplBU3HePC6SHj7kQpZtsoqVU2iqaL2VqVAESYyh5kvCeT1/gYyJCqC/YJyWwiu6yURQhtVh5bplOtSOFJxYtGcDdW/MRb9+lE5RL42E9XHE8dvBWhvXzbPX3BVtqZHaxQx2r10IDojfAG+TNCbHqJJafDHprGumy3vQwTUOqekdYLd4P5+kPCFIpTuEJSEBJbx18DCE04putmydkytE8cpOCFdTaifk5RJq4iJbR8/+laP+jQ7qUKMBwc2+pRAt9cfzh7QyoM9g/RLcuGEhClN9YarzYtQldOCbvzAg0jUg7CWORSjiU0zJ+ZxPKfd45ck= generated-by-azure"
            username = "azureuser"

    }
    size = "Standard_DS1_v2"
    boot_diagnostics {
                storage_account_uri =  ""
              }
            
 provisioner "local-exec" {
    when    = "create"
    command = "ansible-playbook -i hosts --private-key orgerta-vm_key.pem ./react.yaml"
  }

    
}
