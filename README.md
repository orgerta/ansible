# Procedure for docker images sync/replica from Azure Container Registry to Harbor and vice-versa




### Authenticate my credentials with the Azure CLI on local machine




Run the login command.




```bash

$az login

```

 As we have several subscriptions, set the desired subscription the default one (the one where we have ACR)




```bash

$az account set --subscription <subscription-id>

```

Log in to  Azure Container Registry (ACR) instance where we will work 

```bash

$az acr login --name <container_registry_name>

```

 We can retrieve the ACR access keys directly from the Azure portal or by typing the command below

```bash

$az acr credential show --name <container_registry_name>

```

### Login to our Harbor host

```bash

$docker login <harbor_registry_url>

```

Open the Harbor web interface and create a New Project there (here we choose if our project will be public or not)




### Create Replication Endpoint





- [ ] Go to Registries and click the **+ New Endpoint button**

- [ ] Provider field set to **Azure ACR** as we want to replicate an ACR to harbor

- [ ] Endpoint url will be the login server of our ACR in format **"https://loginserver"**

- [ ] Add "username" and "password" values from access keys section of container registry in azure 

- [ ] Test connection should be successful





## Pull-based replication rule




### Create Replication Rule




<img src="https://dl.dropboxusercontent.com/s/2lkqatlg0lp14dk/replica1.png" alt="Replication Rule push-based" width="300" height="300">





- [ ] Since we want to replicate from Azure Container Registry to Harbor , the replication rule should be **Pull-based**

- [ ] As **source registry** ,choose endpoint created in the previous step

- [ ] As **destination namespace** ,give the name of the project we created before 

- [ ] **Trigger mode**  choose between manual and scheduled ,enable rule and save




#### Tag a random image in my local environment or pull one from docker registry 




```bash

$docker pull <image_name>:<tag>

$docker tag <image_name>:<tag> <acr_name>.azurecr.io/<image_name>:<tag>

```

#### Push the tagged image to Azure Container Registry




```bash

$docker push <acr_name>.azurecr.io/<image_name>:<tag>

```

##### Then go to Harbor web interface, select my replication rule pull based and click **replicate**.

##### After few seconds we can see that image we pushed to ACR is replicated on Harbor in our project




## Pushed-based replication rule

 

### Create Replication Rule  

 We can also try the reverse rule , a "Push-based" rule which makes possible that for any image push to our project  in Harbor , it will be replicated in Azure  Container Registry





 <img src="https://www.dropbox.com/s/vxp7tzfc725cd8v/replica2.png?raw=1" alt="Replication Rule pull-based" width="300" height="300">

 




- [ ] **Replication Mode** set to **Push-based**

- [ ] **Source resource filter** set to "harbor_project_name/**"  (means it matches any sequence of characters, including path separators)

- [ ] In **Destination Registry** field is choosen the replication endpoint we created before

- [ ] **Triger Mode** is set to **event-based**




### Test "push-based" replication rule 




#### Tag a random image in our local environment or pull one from docker registry 




```bash

$docker pull <image_name>:<tag>

$docker tag <image_name>:<tag> <harbor_registry_url>/<project_name>/<image_name>:<tag>s

```

#### Push the tagged image to Harbor




```bash

$docker push <harbor_registry_url>/<project_name>/<image_name>:<tag>

```

##### Then go to Harbor web interface, select my replication rule push based and click **replicate**.

#### The image is pushed to Harbor and replicated to container registry on Azure

- As the trigger mode in this rule is choosen "event-based" , we can see that after the image is pushed to Harbor, after a few seconds it is immediately replicated to container registry on Azure
